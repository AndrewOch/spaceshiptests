package reflection;

public class TestObj {


    public String name;
    private boolean isReady;
    private int count;


    public TestObj(String name, boolean isReady, int count) {
        this.name = name;
        this.isReady = isReady;
        this.count = count;
    }

    private TestObj(String name) {

        this.name = name;
    }


    private int addCount(int additive) {
        return count + additive;
    }

    @Override
    public String toString() {
        return "Obj: " +
                name +
                " : " + isReady +
                " : " + count;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
