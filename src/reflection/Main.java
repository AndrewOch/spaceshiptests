package reflection;

import java.lang.reflect.*;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        TestObj obj = new TestObj("Bob", true, 44);
        System.out.println(obj.toString());
        Class<? extends TestObj> aClass = obj.getClass();
        try {
            Field count = aClass.getDeclaredField("count");
            count.setAccessible(true);
            count.set(obj, 4);
            System.out.println(obj.toString());

            Method[] methods = aClass.getMethods();
            System.out.println(Arrays.toString(methods));

            Method addCount = aClass.getDeclaredMethod("addCount", int.class);
            addCount.setAccessible(true);
            System.out.println(addCount.invoke(obj, 5));


            System.out.println("Abstract: " + Modifier.isAbstract(aClass.getModifiers()));
            System.out.println("Final: " + Modifier.isFinal(aClass.getModifiers()));


            Constructor<? extends TestObj> testObj = aClass.getDeclaredConstructor(String.class);
            testObj.setAccessible(true);
            System.out.println(testObj.newInstance("test"));


            Method[] declaredMethods = aClass.getDeclaredMethods();
            Arrays.stream(declaredMethods)
                    .forEach(method -> System.out.println(
                            Arrays.asList(method.getDeclaredAnnotations())));

        } catch (NoSuchFieldException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

    }
}
