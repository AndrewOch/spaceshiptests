package spaceships;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        return ships.stream()
                .filter(spaceship -> spaceship.getFirePower() > 0)
                .max((o1, o2) -> Math.max(o1.getFirePower(), o2.getFirePower()))
                .orElse(null);
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        return ships.stream()
                .filter(spaceship -> spaceship.getName().equals(name)).findFirst()
                .orElse(null);
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        return ships.stream()
                .filter(spaceship -> spaceship.getCargoSpace() >= cargoSize)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        return ships.stream()
                .filter(spaceship -> spaceship.getFirePower() == 0)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public boolean checkIfArmed(ArrayList<Spaceship> ships) {
        return !ships.isEmpty() && ships.stream().filter(Objects::nonNull)
                .allMatch(spaceship -> spaceship.getFirePower() > 0);
    }

    public Long numberOfNamesWithoutNumbers(ArrayList<Spaceship> ships) {
        return ships.stream()
                .filter(spaceship -> !spaceship.getName().contains("1") &&
                        !spaceship.getName().contains("2") && !spaceship.getName().contains("3") &&
                        !spaceship.getName().contains("4") && !spaceship.getName().contains("5") &&
                        !spaceship.getName().contains("6") && !spaceship.getName().contains("7") &&
                        !spaceship.getName().contains("8") && !spaceship.getName().contains("9") &&
                        !spaceship.getName().contains("0"))
                .count();
    }
}
