package spaceships;

import javafx.scene.effect.Shadow;

public class Spaceship implements Comparable<Spaceship> {
    private final String name;

    //огневая мощь
    private Integer firePower;

    //размер грузового трюма
    private Integer cargoSpace;

    //прочность
    private Integer durability;

    public Spaceship(String name, int firePower, int cargoSpace, int durability) {
        this.name = name;
        this.firePower = firePower;
        this.cargoSpace = cargoSpace;
        this.durability = durability;
    }

    public String getName() {
        return name;
    }

    public int getFirePower() {
        return firePower;
    }

    public int getCargoSpace() {
        return cargoSpace;
    }

    public int getDurability() {
        return durability;
    }

    @Override
    public int compareTo(Spaceship o) {
        return this.firePower.compareTo(o.firePower);
    }
}
