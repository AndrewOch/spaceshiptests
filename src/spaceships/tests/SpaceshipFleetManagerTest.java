package spaceships.tests;

import spaceships.CommandCenter;
import spaceships.Spaceship;

import java.util.ArrayList;


public class SpaceshipFleetManagerTest {

    CommandCenter commandCenter = new CommandCenter();
    static float score = 0;


    public static void main(String[] args) {

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest();

        boolean test1_1 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_return();
        boolean test1_2 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_returnFirst();
        boolean test1_3 = spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_returnNull();
        boolean test2_1 = spaceshipFleetManagerTest.getShipByName_shipFound();
        boolean test2_2 = spaceshipFleetManagerTest.getShipByName_shipNotFound();
        boolean test3_1 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnList();
        boolean test3_2 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnBlank();
        boolean test4_1 = spaceshipFleetManagerTest.getAllCivilianShips_returnList();
        boolean test4_2 = spaceshipFleetManagerTest.getAllCivilianShips_returnBlank();

        if (test1_1) {
            System.out.println("test 1.1 ok");
            score += 0.25;
        }

        if (test1_2) {
            System.out.println("test 1.2 ok");
            score += 0.25;
        }

        if (test1_3) {
            System.out.println("test 1.3 ok");
            score += 0.5;
        }

        if (test2_1) {
            System.out.println("test 2.1 ok");
            score += 0.5;
        }
        if (test2_2) {
            System.out.println("test 2.2 ok");
            score += 0.5;
        }
        if (test3_1) {
            System.out.println("test 3.1 ok");
            score += 0.5;
        }
        if (test3_2) {
            System.out.println("test 3.2 ok");
            score += 0.5;
        }
        if (test4_1) {
            System.out.println("test 4.1 ok");
            score += 0.5;
        }
        if (test4_2) {
            System.out.println("test 4.2 ok");
            score += 0.5;
        }

        System.out.println("Total score: " + score + " / 4.0");

    }

    private boolean getMostPowerfulShip_mostPowerfulShipExists_return() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 20, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);

        if (result.getFirePower() == 100) {
            return true;
        }
        return false;
    }

    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnFirst() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 100, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);

        if (result.getFirePower() == 100 && result.getName().equals("Foo")) {
            return true;
        }
        return false;
    }

    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnNull() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 0, 0, 0));
        testShipsList.add(new Spaceship("Doo", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);

        return result == null;
    }

    private boolean getShipByName_shipFound() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 20, 0, 0));

        Spaceship result = commandCenter.getShipByName(testShipsList, "Foo");

        if (result.getName().equals("Foo")) {
            return true;
        }
        return false;
    }

    private boolean getShipByName_shipNotFound() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 20, 0, 0));

        Spaceship result = commandCenter.getShipByName(testShipsList, "Fff");

        if (result == null) {
            return true;
        }
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_returnList() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 20, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 300);

        if (result.get(0).getCargoSpace() >= 300) {
            return true;
        }
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_returnBlank() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 20, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 3000);

        if (result.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean getAllCivilianShips_returnList() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 0, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);

        if (result.get(0).getFirePower() == 0) {
            return true;
        }
        return false;
    }

    private boolean getAllCivilianShips_returnBlank() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 20, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);

        if (result.isEmpty()) {
            return true;
        }
        return false;
    }
}
