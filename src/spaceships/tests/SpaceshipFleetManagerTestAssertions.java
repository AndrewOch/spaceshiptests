package spaceships.tests;

import spaceships.CommandCenter;
import spaceships.Spaceship;
import org.junit.jupiter.api.*;

import java.util.ArrayList;


public class SpaceshipFleetManagerTestAssertions {

    CommandCenter commandCenter = new CommandCenter();
    static ArrayList<Spaceship> testShipsList;

    @BeforeAll
    static void createArrayList() {
        testShipsList = new ArrayList<>();
    }

    @AfterEach
    void cleanList() {
        testShipsList.clear();
    }

    @DisplayName("Возвращает сильнейший корабль")
    @Test
    void getMostPowerfulShip_mostPowerfulShipExists_return() {
        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 20, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        Assertions.assertEquals(result.getFirePower(), 100);

    }

    @DisplayName("Возвращает первый сильнейший корабль из нескольких")
    @Test
    void getMostPowerfulShip_mostPowerfulShipExists_returnFirst() {
        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 100, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        Assertions.assertEquals(result.getName(), "Foo");
    }

    @DisplayName("Возвращает null если сильнейший корабль не найден")
    @Test
    void getMostPowerfulShip_mostPowerfulShipExists_returnNull() {

        testShipsList.add(new Spaceship("Foo", 0, 0, 0));
        testShipsList.add(new Spaceship("Doo", 0, 0, 0));
        Assertions.assertNull(commandCenter.getMostPowerfulShip(testShipsList));
    }

    @DisplayName("Возвращает корабль с нужным именем")
    @Test
    void getShipByName_shipFound() {

        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 20, 0, 0));

        Spaceship result = commandCenter.getShipByName(testShipsList, "Foo");
        Assertions.assertEquals(result.getName(), "Foo");

    }

    @DisplayName("Возвращает null если корабль с нужным именем не найден")
    @Test
    void getShipByName_shipNotFound() {

        testShipsList.add(new Spaceship("Foo", 100, 0, 0));
        testShipsList.add(new Spaceship("Doo", 20, 0, 0));

        Assertions.assertNull(commandCenter.getShipByName(testShipsList, "Fff"));
    }

    @DisplayName("Возвращает список кораблей с нужной грузоподъёмностью")
    @Test
    void getAllShipsWithEnoughCargoSpace_returnList() {

        Spaceship testShip1 = new Spaceship("Foo", 100, 400, 0);
        Spaceship testShip2 = new Spaceship("Dgg", 50, 350, 0);

        testShipsList.add(testShip1);
        testShipsList.add(new Spaceship("Doo", 20, 200, 0));
        testShipsList.add(testShip2);

        ArrayList<Spaceship> test = new ArrayList<>();

        test.add(testShip1);
        test.add(testShip2);

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 300);
        Assertions.assertArrayEquals(new ArrayList[]{result}, new ArrayList[]{test});

    }

    @DisplayName("Возвращает пустой список, если кораблей с нужной грузоподъёмностью нет")
    @Test
    void getAllShipsWithEnoughCargoSpace_returnBlank() {
        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 20, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 3000);
        Assertions.assertTrue(result.isEmpty());

    }

    @DisplayName("Возвращает список гражданских кораблей")
    @Test
    void getAllCivilianShips_returnList() {

        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 0, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        Assertions.assertEquals(result.get(0).getName(), "Doo");

    }

    @DisplayName("Возвращает пустой список, если гражданских кораблей нет")
    @Test
    void getAllCivilianShips_returnBlank() {

        testShipsList.add(new Spaceship("Foo", 100, 400, 0));
        testShipsList.add(new Spaceship("Doo", 20, 200, 0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        Assertions.assertTrue(result.isEmpty());

    }

    @Test
    @DisplayName("Возвращает false, если список имеет невооружённые корабли")
    void checkIfArmed_returnFalse() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Loo", 0, 40, 3));
        boolean result = commandCenter.checkIfArmed(testShipsList);
        Assertions.assertFalse(result);

    }


    @Test
    @DisplayName("Возвращает true, если в списке все вооружённые корабли")
    void checkIfArmed_returnTrue() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Loo", 20, 40, 3));
        boolean result = commandCenter.checkIfArmed(testShipsList);
        Assertions.assertTrue(result);
    }


    @Test
    @DisplayName("Возвращает false, если передан пустой список")
    void checkIfArmed_returnFalseIfEmpty() {
        testShipsList = new ArrayList<>();
        boolean result = commandCenter.checkIfArmed(testShipsList);
        Assertions.assertFalse(result);
    }


    @Test
    @DisplayName("Возвращает количество кораблей, у которых в имени нет цифры")
    void numberOfNamesWithoutNumbers_returnNumber() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("F4oo", 100, 50, 3));
        testShipsList.add(new Spaceship("Loo", 20, 40, 3));
        Long result = commandCenter.numberOfNamesWithoutNumbers(testShipsList);
        Assertions.assertEquals(result, 1);
    }


    @Test
    @DisplayName("Возвращает 0, если передан пустой список")
    void numberOfNamesWithoutNumbers_returnZero() {
        testShipsList = new ArrayList<>();
        Long result = commandCenter.numberOfNamesWithoutNumbers(testShipsList);
        Assertions.assertEquals(result, 0);
    }

    @Test
    @DisplayName("Возвращает 0, если у всех кораблей в именах цифры")
    void numberOfNamesWithoutNumbers_returnZeroIfEmpty() {
        testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("F4oo", 100, 50, 3));
        testShipsList.add(new Spaceship("L4oo", 20, 40, 3));
        Long result = commandCenter.numberOfNamesWithoutNumbers(testShipsList);
        Assertions.assertEquals(result, 0);

    }
}
