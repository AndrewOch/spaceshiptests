package strategyGame.entities;

public abstract class Unit {

    public Unit() {
    }

    abstract String generateName();
}
