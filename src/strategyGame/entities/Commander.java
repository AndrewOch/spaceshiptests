package strategyGame.entities;

import strategyGame.RandomNameGenerator;
import strategyGame.annotations.GenerateMinions;
import strategyGame.annotations.InvokeMethods;
import strategyGame.annotations.Randomize;

import java.util.List;

public class Commander extends Unit {
    @InvokeMethods(name = "generateName")
    String name;

    @Randomize(min = 20, max = 70)
    int hp;
    @Randomize(min = 5, max = 20)
    int dmg;
    @Randomize(min = 10, max = 40)
    int mana;
    @Randomize(min = 2, max = 6)
    int str;
    @Randomize(min = 2, max = 6)
    int agl;

    @GenerateMinions()
    List<Unit> minions;

    @Override
    public String toString() {
        return "Коммандир: " + name + " | hp: " + hp +
                " | dmg: " + dmg +
                " | mana: " + mana +
                " | str: " + str +
                " | agl: " + agl +
                "\nПодчинённые: " + minions;
    }

    String generateName() {
        RandomNameGenerator randomNameGenerator = new RandomNameGenerator();
        return randomNameGenerator.generateName(this);
    }
}
