package strategyGame;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Random;

public class RandomNameGenerator {


    Random random = new Random();
    int firstNamePartsCount;
    int secondNamePartsCount;

    public String generateName(Object o) {
        String first = null;
        String second = null;
        Class<?> aClass = o.getClass();
        Field field = null;
        try {
            field = aClass.getDeclaredField("name");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        if (field != null) {
            try {
                int i = random.nextInt(firstNamePartsCount) + 1;

                FileReader fileReader = new FileReader("src\\strategyGame\\files\\firstNameParts");
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                for (int j = 0; j < i; j++) {
                    first = bufferedReader.readLine();
                }
                bufferedReader.close();
                fileReader.close();

                i = random.nextInt(secondNamePartsCount) + 1;

                fileReader = new FileReader("src\\strategyGame\\files\\secondNameParts");
                bufferedReader = new BufferedReader(fileReader);
                for (int j = 0; j < i; j++) {
                    second = bufferedReader.readLine();
                }

                bufferedReader.close();
                fileReader.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return first + second;
    }


    public RandomNameGenerator() {
        checkNameFiles();
    }

    private void checkNameFiles() {

        try {
            FileReader fileReader = new FileReader("src\\strategyGame\\files\\firstNameParts");

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            int i = 0;
            while (bufferedReader.readLine() != null) {

                i++;
            }

            firstNamePartsCount = i;

            fileReader = new FileReader("src\\strategyGame\\files\\secondNameParts");

            bufferedReader = new BufferedReader(fileReader);
            i = 0;
            while (true) {
                try {
                    if (bufferedReader.readLine() == null) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                i++;
            }
            secondNamePartsCount = i;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
