package strategyGame;

import strategyGame.annotations.GenerateMinion;
import strategyGame.annotations.GenerateMinions;
import strategyGame.annotations.InvokeMethods;
import strategyGame.annotations.Randomize;
import strategyGame.entities.Unit;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class UnitFabric {

    Random random = new Random();

    public Unit createUnitByName(String unitClass) throws ClassNotFoundException {

        Class<?> aClass = null;

            aClass = Class.forName(unitClass);

        Class<?> ph = aClass;
        while (ph != Unit.class) {
            if (ph.getSuperclass() == null) {
                throw new ClassNotFoundException();
            } else {
                ph = ph.getSuperclass();
            }
        }
        try {
            Constructor<?> constructor = aClass.getConstructor();
            Unit o = (Unit) constructor.newInstance();
            Field[] declaredFields = aClass.getDeclaredFields();
            Arrays.stream(declaredFields).forEach(field -> {
                field.setAccessible(true);
                Randomize randomize = field.getAnnotation(Randomize.class);
                if (randomize != null) {
                    randomizeFields(o, field, randomize.min(), randomize.max());
                }
                InvokeMethods invoke = field.getAnnotation(InvokeMethods.class);
                if (invoke != null) {
                    invokeMethods(o, field, invoke.name());
                }
                GenerateMinion generateMinion = field.getAnnotation(GenerateMinion.class);
                if (generateMinion != null) {
                    try {
                        field.set(o, createUnitByName("strategyGame.entities.Golem"));
                    } catch (IllegalAccessException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                GenerateMinions generateMinions = field.getAnnotation(GenerateMinions.class);
                if (generateMinions != null) {
                    try {
                        generateMinions(o, field, generateMinions);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                field.setAccessible(false);
            });

            return o;
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
        throw new ClassNotFoundException();
    }

    public List<Unit> createUnitsByName(String unitClass, int count) throws ClassNotFoundException {
        return getUnits(count, unitClass);
    }

    private void invokeMethods(Object o, Field field, String name) {
        try {
            Method declaredMethod = o.getClass().getDeclaredMethod(name);
            field.setAccessible(true);
            declaredMethod.setAccessible(true);
            field.set(o, declaredMethod.invoke(o));
            field.setAccessible(false);
            declaredMethod.setAccessible(false);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void randomizeFields(Object o, Field field, int min, int max) {
        try {
            field.setAccessible(true);
            field.set(o, min +
                    random.nextInt(max - min));
            field.setAccessible(false);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void generateMinions(Object o, Field field, GenerateMinions generateMinions) throws ClassNotFoundException {
        try {
            int r = random.nextInt(generateMinions.maxCount()
                    - generateMinions.minCount()) + generateMinions.minCount();
            field.setAccessible(true);
            field.set(o, getUnits(r, generateMinions.unitClass()));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private List<Unit> getUnits(int count, String unitClass) throws ClassNotFoundException {
        List<Unit> units = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            units.add(createUnitByName(unitClass));
        }
        return units;
    }

}