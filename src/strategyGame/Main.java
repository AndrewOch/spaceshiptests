package strategyGame;

import strategyGame.entities.Unit;

import java.util.List;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException {

        UnitFabric unitFabric = new UnitFabric();

        Unit warrior = unitFabric.createUnitByName("strategyGame.entities.Warrior");
        Unit archer = unitFabric.createUnitByName("strategyGame.entities.Archer");
        Unit mage = unitFabric.createUnitByName("strategyGame.entities.Mage");
        Unit warrior1 = unitFabric.createUnitByName("strategyGame.entities.Warrior");
        Unit archer1 = unitFabric.createUnitByName("strategyGame.entities.Archer");
        Unit mage1 = unitFabric.createUnitByName("strategyGame.entities.Mage");
        Unit commander = unitFabric.createUnitByName("strategyGame.entities.Commander");

        List<Unit> units = unitFabric.createUnitsByName("strategyGame.entities.Mage", 5);

        System.out.println(warrior);
        System.out.println(archer);
        System.out.println(mage);
        System.out.println(commander);
        System.out.println(warrior1);
        System.out.println(archer1);
        System.out.println(mage1);

        System.out.println("\n");

        units.forEach(System.out::println);

        List<Unit> units1 = unitFabric.createUnitsByName("strategyGame.entities.Commander", 5);

        units1.forEach(System.out::println);

        Unit unit = unitFabric.createUnitByName("strategyGame.entities.Imposter");
    }
}
