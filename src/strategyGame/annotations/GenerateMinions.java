package strategyGame.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface GenerateMinions {
    String unitClass() default "strategyGame.entities.Warrior";

    int minCount() default 0;

    int maxCount() default 5;
}
