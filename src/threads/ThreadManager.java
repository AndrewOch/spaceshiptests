package threads;

import java.util.Random;

public class ThreadManager {

    public static void main(String[] args) {
        new Thread1("First").run();
        new Thread1("Second").run();
        new Thread1("Third").run();
    }

}

class Thread1 implements Runnable {
    public Thread1(String name) {
        this.name = name;
    }

    String name;

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(name + " is running!");
            try {
                Thread.sleep(new Random().nextInt(900) + 100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
}