package threads;

public class TicTac {
    public static void main(String[] args) {
        new TicTac().go();
    }

    public void go() {
        new Thread(new Tic()).start();
        new Thread(new Tac()).start();
    }

    private synchronized void tic() {
        try {
            Thread.sleep(500);
            System.out.println("Tic");
            notify();
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private synchronized void tac() {
        try {
            Thread.sleep(500);
            System.out.println("Tac");
            notify();
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    class Tic implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                tic();
            }
        }
    }


    class Tac implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                tac();
            }
        }
    }
}