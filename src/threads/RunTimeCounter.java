package threads;

import java.util.Random;

public class RunTimeCounter {
    public static void main(String[] args) throws InterruptedException {
        Thread runner = new Thread(new Runner());
        runner.start();
        Thread.sleep(5000);
        if (runner.isAlive()) {
            runner.interrupt();
            System.out.println("Failure");
        } else {
            System.out.println("Success");
        }
    }
}

class Runner implements Runnable {

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            try {
                int time = random.nextInt(1000) + 500;
                Thread.sleep(time);
                System.out.println(time);

            } catch (InterruptedException e) {
                break;
            }

        }
    }
}


